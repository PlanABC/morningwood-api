import {apiData} from "../../../data/data"


export default function handler(req, res) {
    const { slug } = req.query
    const forest = apiData.forests.find(forest => forest.url === slug)
    res.status(200).json(forest);
  }